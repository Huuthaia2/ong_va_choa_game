using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ong : MonoBehaviour
{
    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    public GameObject _target;
    public float _speed;
    public float _doNay;
    Rigidbody2D r2d;
    private void OnEnable()
    {
        _target = GameObject.Find("Cho");
        r2d =GetComponent<Rigidbody2D>();
        run();
    }
    void run(){
        Vector2 dis = _target.transform.position - transform.position;
        Debug.Log(dis);
        dis.Normalize();
        Debug.Log("Normalize:: "+dis);
        //transform.LookAt(_target.transform.position,Vector3.right);
        r2d.AddForce(dis*_speed);
        transform.right = dis;
    }
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    private void Update()
    {
        run(); 
    }

    void NayRa(float doNay){
        Vector2 dis = _target.transform.position - transform.position;
        Debug.Log(dis);
        dis.Normalize();
        Debug.Log("Normalize:: "+dis); 
        r2d.AddForce(dis*_speed*(-1)*doNay);
    }
    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag != "Ong"){
            NayRa(_doNay);
        }else{
            NayRa(3.5f);
        }
        if(other.gameObject.name=="Cho"){
            //


            run();
        }
    }
}

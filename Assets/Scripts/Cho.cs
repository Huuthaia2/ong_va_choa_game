using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cho : MonoBehaviour
{
    
    public GameObject EndUI;
    public float time;
    public void countDown_Win(){
        EndUI.transform.GetChild(0).GetChild(0).GetComponent<Text>().text="You Win";
        StartCoroutine(ShowEnd(time));
    }
    
    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag=="Ong"){
            EndUI.transform.GetChild(0).GetChild(0).GetComponent<Text>().text="You Lose";
            StartCoroutine(ShowEnd(1.75f));
        }
    }
    IEnumerator ShowEnd(float delay){
        yield return new WaitForSeconds(delay);
        EndUI.SetActive(true);
    }
}

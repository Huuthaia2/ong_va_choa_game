using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToOng : MonoBehaviour
{

    public GameObject _ong;
    public int _number;
    public Transform target;

    public float timeSpawn;
    // Start is called before the first frame update
    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    float _time;
    int _count=0;
    private void OnEnable()
    {
        _time=timeSpawn/(_number-1);
        //StartCoroutine(SpawnOng());
    }

    public void RunSpawn(){
        StartCoroutine(SpawnOng());
    }
    IEnumerator SpawnOng(){
       yield return new WaitForSeconds(_time);
        GameObject ong= Instantiate(_ong,transform.position,Quaternion.identity);
        ong.transform.parent = transform;
        ong.transform.localScale=new Vector3(1,1,1);
        _count++;
        if(_count<=_number){
            StartCoroutine(SpawnOng());
        }
    }
 
    // Update is called once per frame
    void Update()
    {
        
    }
}
